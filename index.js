
// console.log("Add Post")
let posts = [];
let count = 1;
// Add post data
	// addEventListener(event, callback function that witll be triggere if the event occur or happen)
	document.querySelector("#form-add-post").addEventListener("submit", 
		(event) => {
			/*prevent Default function stops the auto reload of the webpage when submitting*/
			event.preventDefault();
			posts.push({
				id: count,
				title: document.querySelector("#txt-title").value,
				body: document.querySelector("#txt-body").value
			})
			count++;
			showPosts(posts);
		})
// Show posts
	const showPosts = (posts) =>{
		let postEntries = '';
		posts.forEach((post) =>{
			postEntries += `<div id = "post-${post.id}">
			<h3 id = "post-title-${post.id}">${post.title}</h3>
			<p id = "post-body-${post.id}">${post.body}</p>
			<button onclick="editPost('${post.id}')" >Edit</button>
			<button onclick="deletePost('${post.id}')">Delete</button>
			</div>`
		})
		document.querySelector("#div-post-entries").innerHTML = postEntries;
	}
// Edit post
const editPost = (id)=>{
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
    document.querySelector(`#form-edit-post`).setAttribute("data-editId",id)
}

const deletePost = (event,id)=>{
    posts.splice(id-1, 1)
	showPosts(posts);
    alert('Delete is successful');
}

// Add post Form Submit
document.querySelector("#form-edit-post").addEventListener("submit", (event) => {
	event.preventDefault();
	    const idToBeEdited=document.querySelector('#form-edit-post').getAttribute("data-editId")
	for( let i = 0; i < posts.length; i++){
		if(posts[i].id.toString() === idToBeEdited){
			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].body = document.querySelector('#txt-edit-body').value;
			showPosts(posts);
			alert('Edit is successful');
			break;
		}
	}
})
